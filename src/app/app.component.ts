import { Component} from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { ThemeService } from './services/theme-service.service';
import { UserStore } from './store/user.store';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'auto-ship';
  isThemeDark: Observable<boolean>= new BehaviorSubject(true);
  
  constructor(
    private themeService: ThemeService,
    private userStore:UserStore
  ) {}
  async ngOnInit() {
    this.themeService.setDarkTheme(true);
    await this.userStore.getCurrentUser().then(()=>{
     }).catch((error)=>console.log(error))
   }
   
  toggleDarkTheme(checked) {
    this.themeService.setDarkTheme(checked.checked);
  }
}
