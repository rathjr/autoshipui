import { Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { deployStatus } from 'src/app/dummy/status';
import { IDatabase } from 'src/app/interfaces/db.interface';
import { DatabaseStore } from 'src/app/store/database.store';
import { FrontEndStore } from 'src/app/store/frontend.store';
import { UserStore } from 'src/app/store/user.store';

@Component({
  selector: 'app-add-database',
  templateUrl: './add-database.component.html',
  styleUrls: ['./add-database.component.scss']
})
export class AddDatabaseComponent implements OnInit {
  form: FormGroup;
  name: AbstractControl;
  description: AbstractControl;
  username: AbstractControl;
  password: AbstractControl;
  connection_limit:AbstractControl;
  selected_value:AbstractControl;
  port:AbstractControl;
  postgreSQL='../../../assets/images/postgresql+plain+wordmark-1324760555518154961_512.png'
  mySQL='../../../assets/images/icons8-mysql-logo-240.png'
  currentLogoSelected='../../../assets/images/postgresql+plain+wordmark-1324760555518154961_512.png'
  db_types: string[] = ['PostgreSQL', 'MySQL',];
  @ViewChild('focusInput') inputEl: ElementRef;
  constructor(
    public dialogRef: MatDialogRef<AddDatabaseComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private store:DatabaseStore,
    private snackBar:MatSnackBar,
    private userStore:UserStore,
    private frontEndSotre:FrontEndStore

  ) { }
  buildForm(): void {
    this.form = this.fb.group({
      name: [this.data.item ? this.data.item.name : null,Validators.required],
      description: [this.data.item ? this.data.item.description : null],
      username: [this.data.item ? this.data.item.username : null,Validators.required],
      password: [this.data.item ? this.data.item.password : null,Validators.required],
      connection_limit: [this.data.item ? this.data.item.connection_limit : null],
      port: [this.data.item ? this.data.item.port : null,Validators.required],

      selected_value: [this.data.item ? this.data.item.selected_value : this.db_types[0]],
    });
    this.name = this.form.controls['name'];
    this.description = this.form.controls['description'];
    this.username = this.form.controls['username'];
    this.password = this.form.controls['password'];
    this.connection_limit = this.form.controls['connection_limit'];
    this.port = this.form.controls['port'];
    this.selected_value = this.form.controls['selected_value'];
  }
  ngOnInit() {
    this.buildForm()
  }
  onChangeDBType(type:string){
    this.selected_value.setValue(type)
    if(this.selected_value.value=='PostgreSQL'){
      this.currentLogoSelected=this.postgreSQL
    }else{
      this.currentLogoSelected=this.mySQL
    }
  }
  onSave(){
    let owner_key=this.userStore.userID
    if(owner_key && !this.form.valid){
      this.username.markAsTouched();
      this.password.markAsTouched();
      this.port.markAllAsTouched();
      this.snackBar.open('Datbase information required!','error',{
        duration:4000
      });
    }else{
      let item:IDatabase={
      key:this.store.firebaseKey().toLowerCase(),
      owner_key:owner_key,
      db_name:this.name.value,
      container_name:this.name.value.toLowerCase(),
      db_type:this.selected_value.value,
      description:this.description.value,
      username:this.username.value,
      password:this.password.value,
      connection_limit:this.connection_limit.value?this.connection_limit.value:100,
      status:deployStatus[0],
      record_active_status:true,
      ip:'34.126.133.229',
      port:this.port.value,
      create_by:owner_key,
      update_by:owner_key,
      create_date:this.store.dateTime(),
      update_date:this.store.dateTime()      
    }
    this.store.add(item).then(()=>{
      this.dialogRef.close()
      this.frontEndSotre.getDatabaseSize()
      this.snackBar.open(`You're ${item?.db_name} database has been created`,'Done',{
        duration:4000
      })
    })
    .catch((error)=>console.log(error))
  }
}
}
