import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-app-detail',
  templateUrl: './app-detail.component.html',
  styleUrls: ['./app-detail.component.scss']
})
export class AppDetailComponent implements OnInit {
  @Input() info:any;
  accessAddress=null
  constructor(
    public dialogRef: MatDialogRef<AppDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit(): void {
    let url=this.data.item.ip
    let port=this.data.item.port
    this.accessAddress=`${url}:${port}`
    console.log(this.accessAddress)
  }
  goToLink(){
    window.open(this.accessAddress, "_blank");
}
}
