import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DatabaseStore } from 'src/app/store/database.store';

@Component({
  selector: 'app-database-detail',
  templateUrl: './database-detail.component.html',
  styleUrls: ['./database-detail.component.scss']
})

export class DatabaseDetailComponent implements OnInit {
  accessAddress=null
  constructor(
    public dialogRef: MatDialogRef<DatabaseDetailComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }
  ngOnInit(): void {
    let url=this.data.item.ip
    let port=this.data.item.port
    this.accessAddress=`${url}:${port}`
    console.log(this.accessAddress)
  }

}
