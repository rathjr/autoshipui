import { HttpClient, HttpEventType } from '@angular/common/http';
import { Component, ElementRef, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Subscription } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { buildTool, deployStatus, jdkVersion, nodejsVersion } from 'src/app/dummy/status';
import { IDeploySpring } from 'src/app/interfaces/deploySpring.interface';
import { IFrontEnd } from 'src/app/interfaces/frontend.interface';
import { FrontEndStore } from 'src/app/store/frontend.store';
import { UserStore } from 'src/app/store/user.store';

@Component({
  selector: 'app-add-applications',
  templateUrl: './add-applications.component.html',
  styleUrls: ['./add-applications.component.scss']
})
export class AddApplicationsComponent implements OnInit {
  form: FormGroup;
  name: AbstractControl;
  description: AbstractControl;
  gitUrl: AbstractControl;
  // password: AbstractControl;
  selected_app_type_value: AbstractControl;
  selected_source_code_value: AbstractControl;
  nodejs_version: AbstractControl;
  jdk_version: AbstractControl;
  build_tool: AbstractControl;
  spring_build_file: AbstractControl;

  reactJS = '../../../assets/images/icons8-react-native-480.png'
  spring = '../../../assets/images/icons8-spring-logo-480.png'
  angular = '../../../assets/images/4373284_angular_logo_logos_icon.png'
  isUpload = false
  currentLogoSelected = '../../../assets/images/icons8-react-native-480.png'
  application_types: string[] = ['ReactJS', 'Angular', 'Spring'];
  source_code_types: string[] = ['Github', 'Gitlab', 'Upload'];
  springBuiltFiles: string[] = ['Jar', 'War'];

  nodeJSVersion = nodejsVersion
  jdkVersion = jdkVersion
  buildTool = buildTool
  @Input()
  requiredFileType: string;

  fileName = '';
  uploadProgress: number=0;
  uploadSub: Subscription;

  @ViewChild('focusInput') inputEl: ElementRef;

  constructor(
    public dialogRef: MatDialogRef<AddApplicationsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private http: HttpClient,
    public store:FrontEndStore,
    private snackBar:MatSnackBar,
    private userStore:UserStore,
    private frontEndSotre:FrontEndStore

  ) { }
  buildForm(): void {
    this.form = this.fb.group({
      name: [this.data.item ? this.data.item.name : null,Validators.required],
      description: [this.data.item ? this.data.item.description : null],
      // password: [this.data.item ? this.data.item.password : null,Validators.required],

      selected_app_type_value: [this.data.item ? this.data.item.selected_app_type_value : this.application_types[0]],

      selected_source_code_value: [this.data.item ? this.data.item.selected_source_code_value : this.source_code_types[0]],
      gitUrl: [this.data.item ? this.data.item.gitUrl : null,Validators.nullValidator],

      nodejs_version: [this.data.item ? this.data.item.nodejs_version : nodejsVersion[0]],
      jdk_version: [this.data.item ? this.data.item.jdk_version : nodejsVersion[0]],
      build_tool: [this.data.item ? this.data.item.build_tool : buildTool[0]],
      spring_build_file: [this.data.item ? this.data.item.spring_build_file : this.springBuiltFiles[0]],

    });
    this.name = this.form.controls['name'];
    this.description = this.form.controls['description'];
    this.gitUrl = this.form.controls['gitUrl'];
    this.selected_app_type_value = this.form.controls['selected_app_type_value'];
    this.selected_source_code_value = this.form.controls['selected_source_code_value'];
    this.nodejs_version = this.form.controls['nodejs_version'];
    this.jdk_version = this.form.controls['jdk_version'];
    this.build_tool = this.form.controls['build_tool'];
    this.spring_build_file = this.form.controls['spring_build_file'];
  }
  ngOnInit(): void {
    this.buildForm()
  }
  compareWithFn(item1, item2) {
    return item1 && item2 ? item1.key === item2.key : item1 === item2;
  }
  onChangeApplicationType(type: string) {
    this.selected_app_type_value.setValue(type)
    if (this.selected_app_type_value.value == 'ReactJS') {
      this.currentLogoSelected = this.reactJS
    } else if (this.selected_app_type_value.value == 'Angular') {
      this.currentLogoSelected = this.angular
    } else if (this.selected_app_type_value.value == 'Spring') {
      this.currentLogoSelected = this.spring
    }
  }
  onChangeSourceCodeType(type) {
    this.selected_source_code_value.setValue(type);
    if (this.selected_source_code_value.value == 'Upload') {
      this.isUpload = true
    } else {
      this.isUpload = false
    }
  }
  onFileSelected(event) {
    const file: File = event.target.files[0];
    if (file) {
      this.fileName = file.name;
      const formData = new FormData();
      formData.append("thumbnail", file);

      const upload$ = this.http.post("/api/thumbnail-upload", formData, {
        reportProgress: true,
        observe: 'events'
      })
        .pipe(
          finalize(() => this.reset())
        );

      this.uploadSub = upload$.subscribe(event => {
        if (event.type == HttpEventType.UploadProgress) {
          this.uploadProgress = Math.round(100 * (event.loaded / event.total));
        }
      })
    }
  }

  cancelUpload() {
    if(this.uploadSub){
      this.uploadSub.unsubscribe();
      this.reset();
    }else{
      console.log("No file is being upload")
    }

  }

  reset() {
    this.uploadProgress = null;
    this.uploadSub = null;
  }
  onSave(){
    let owner_key=this.userStore.userID
    if(owner_key && !this.form.valid){
      this.gitUrl.markAsTouched();
      this.snackBar.open('Application information is required!','error',{
        duration:4000
      });
    }else{
      if (this.selected_app_type_value.value=="ReactJS" || this.selected_app_type_value.value=="Angular"){
        let deployFrontEnd:IFrontEnd={
          key:this.store.firebaseKey().toLowerCase(),
          app_name:this.name.value,
          container_name:this.name.value.toLowerCase(),
          description:this.description.value,
          owner_key:owner_key,
          language:this.selected_app_type_value.value,
          source_code:this.gitUrl.value,
          nodejs_version:this.nodejs_version.value,
          status:deployStatus[0],
          ip:'34.126.133.229',
          port:'8081',
          record_active_status:true,
          create_by:owner_key,
          update_by:owner_key,
          create_date:this.store.dateTime(),
          update_date:this.store.dateTime(),
          logs:'No log'    
        }
        this.store.add(deployFrontEnd).then(()=>{
          this.dialogRef.close()
          this.frontEndSotre.getDeploySize()
          this.snackBar.open(`You're ${deployFrontEnd?.app_name} application has been created`,'Done',{
            duration:4000
          })
        })
        .catch((error)=>{console.log(error)})
      }else{
        let deploySpring:IDeploySpring={
          key:this.store.firebaseKey().toLowerCase(),
          app_name:this.name.value,
          container_name:this.name.value.toLowerCase(),
          description:this.description.value,
          owner_key:owner_key,
          language:this.selected_app_type_value.value,
          source_code:this.gitUrl.value,
          build_tool:this.build_tool.value,
          jdk_version:this.jdk_version.value,
          status:deployStatus[0],
          build_file:this.spring_build_file.value,
          record_active_status:true,
          ip:'34.126.133.229',
          port:'8085',
          create_by:owner_key,
          update_by:owner_key,
          create_date:this.store.dateTime(),
          update_date:this.store.dateTime(),
          logs:'No log'    
        }
        this.store.add(deploySpring).then(()=>{
          this.frontEndSotre.getDeploySize()
          this.dialogRef.close();
          this.snackBar.open(`You're ${deploySpring?.app_name} application has been created`,'Done',{
            duration:4000
          })
        })
        .catch((error)=>{console.log(error)})
      }
    }
   

  }
}
