import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AddApplicationsComponent } from 'src/app/dialogs/add-applications/add-applications.component';
import { AddDatabaseComponent } from 'src/app/dialogs/add-database/add-database.component';
import { AppDetailComponent } from 'src/app/dialogs/app-detail/app-detail.component';
import { DatabaseDetailComponent } from 'src/app/dialogs/database-detail/database-detail.component';
import { DatabaseStore } from 'src/app/store/database.store';
import { FrontEndStore } from 'src/app/store/frontend.store';
import { UserStore } from 'src/app/store/user.store';
interface IFeature {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-app-listing-item',
  templateUrl: './app-listing-item.component.html',
  styleUrls: ['./app-listing-item.component.scss']
})

export class AppListingItemComponent implements OnInit {
  form: FormGroup;
  features: IFeature[] = [
    {value: 'All Applications', viewValue: 'All Applications'},
    {value: 'Database', viewValue: 'Database'},
    {value: 'Angular', viewValue: 'Angular'},
    {value: 'ReactJS', viewValue: 'ReactJS'},
    {value: 'Spring', viewValue: 'Spring'}

  ];
  featureControl = new FormControl(this.features[0].value);
  currentLanguage=''
  constructor(
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public appStore:FrontEndStore,
    public dbStore:DatabaseStore,
    private userStore:UserStore

    ) { 
    this.form = new FormGroup({
      feature: this.featureControl,
    });
  }
async ngOnInit() {
  //  await this.userStore.getCurrentUser().then(()=>{
   
  //   }).catch((error)=>console.log(error))
  if(this.userStore.userID){
    this.dbStore.fetchData(this.userStore.userID)
    this.appStore.fetchData(this.userStore.userID)
  }
  }
  ngOnDestroy() {
    this.dbStore.fetchData(this.userStore.userID).unsubscribe();
    this.appStore.fetchData(this.userStore.userID).unsubscribe();
    this.appStore.fetchDataByLanguage(this.userStore.userID,this.currentLanguage).unsubscribe()
  }

  onFilterApp(language:string){
    if(language!='Database' && language!='All Applications'){
      this.currentLanguage=language
      this.appStore.fetchDataByLanguage(this.userStore.userID,language)
    }
  }
  addDatabase() {
    const dialogRef = this.dialog.open(AddDatabaseComponent, {
      data: { 
        TITLE:"Create Database",
        item:null,
       },
       minWidth: '50vw',
       height: '85vh',
       disableClose: true,
       role: 'dialog',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result !== 'no') {
       
      }
    });
  }
  addApplication() {
    const dialogRef = this.dialog.open(AddApplicationsComponent, {
      data: { 
        TITLE:"Deploy Application",
        item:null,
       },
       minWidth: '50vw',
       height: '90vh',
       disableClose: true,
       role: 'dialog',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result !== 'no') {
       
      }
    });
  }
  openDB(item){
    if(item=="Database"){
      const dialogRef = this.dialog.open(DatabaseDetailComponent, {
        data: { 
          TITLE:`${item?item:"Unknown"}'S DETAIL`,
          item:null,
         },
         minWidth: '50vw',
         height: '60vh',
         disableClose: false,
         role: 'dialog',
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result && result !== 'no') {
         
        }
      });
    }
    
  }
  openDetail(appname?:string){
    const dialogRef = this.dialog.open(AppDetailComponent, {
      data: { 
        TITLE:`${appname?appname:"Unknown"}'S DETAIL`,
        item:null,
       },
       minWidth: '50vw',
       height: '60vh',
       disableClose: false,
       role: 'dialog',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result !== 'no') {
       
      }
    });
  }


}
