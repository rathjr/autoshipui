import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppListingItemComponent } from './app-listing-item.component';

describe('AppListingItemComponent', () => {
  let component: AppListingItemComponent;
  let fixture: ComponentFixture<AppListingItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppListingItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppListingItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
