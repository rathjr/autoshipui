import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-database-card',
  templateUrl: './database-card.component.html',
  styleUrls: ['./database-card.component.scss']
})
export class DatabaseCardComponent implements OnInit {

  @Input() count:any;

  constructor() { }

  ngOnInit(): void {
  }

}
