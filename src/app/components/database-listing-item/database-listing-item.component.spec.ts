import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatabaseListingItemComponent } from './database-listing-item.component';

describe('DatabaseListingItemComponent', () => {
  let component: DatabaseListingItemComponent;
  let fixture: ComponentFixture<DatabaseListingItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatabaseListingItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DatabaseListingItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
