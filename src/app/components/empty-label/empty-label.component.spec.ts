import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmptyLabelComponent } from './empty-label.component';

describe('EmptyLabelComponent', () => {
  let component: EmptyLabelComponent;
  let fixture: ComponentFixture<EmptyLabelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmptyLabelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EmptyLabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
