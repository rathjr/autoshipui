import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppDetailComponent } from 'src/app/dialogs/app-detail/app-detail.component';
import { ConfirmDialogComponent } from 'src/app/dialogs/confirm-dialog/confirm-dialog.component';
import { FrontEndStore } from 'src/app/store/frontend.store';

@Component({
  selector: 'app-application',
  templateUrl: './application.component.html',
  styleUrls: ['./application.component.scss']
})
export class ApplicationComponent implements OnInit {
  @Input() item:any;
  reactjsLogo='../../../assets/images/icons8-react-native-480.png'
  angularLogo='../../../assets/images/angular-logo.png'
  springLogo='../../../assets/images/icons8-spring-logo-480.png'

  constructor(
    private appStore:FrontEndStore,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit(): void {
  }
  onDelete(item){
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { 
        TITLE:`Are you sure to delete ${item.app_name}?`,
        item:null,
       },
       disableClose: true,
       role: 'dialog',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result !== 'no') {
        this.appStore.delete(item,cb=>{
          console.log(cb)
        })
      }
    });
  }
  onOpenAppDetail(item){
    const dialogRef = this.dialog.open(AppDetailComponent, {
      data: { 
        TITLE:`${item.app_name?item.app_name:"Unknown"}'s Detail`,
        item:item,
       },
       disableClose: true,
       width:"60vw",
       height:"70vh",
       role: 'dialog',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result !== 'no') {
        this.appStore.delete(item,cb=>{
          console.log(cb)
        })
      }
    });
  }
}
