import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ConfirmDialogComponent } from 'src/app/dialogs/confirm-dialog/confirm-dialog.component';
import { DatabaseDetailComponent } from 'src/app/dialogs/database-detail/database-detail.component';
import { DatabaseStore } from 'src/app/store/database.store';

@Component({
  selector: 'app-database',
  templateUrl: './database.component.html',
  styleUrls: ['./database.component.scss']
})
export class DatabaseComponent implements OnInit {
  @Input() item:any;
  postgreSQLLogo='../../../assets/images/postgresql+plain+wordmark-1324760555518154961_512.png'
  mySQLLogo='../../../assets/images/icons8-mysql-logo-240.png'

  constructor(
    private dbStore:DatabaseStore,
    public dialog: MatDialog,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) { }

  ngOnInit(): void {
  }
  onOpenAppDetail(item){
    const dialogRef = this.dialog.open(DatabaseDetailComponent, {
      data: { 
        TITLE:`${item.db_name?item.db_name:"Unknown"}'s Detail`,
        item:item,
       },
       disableClose: true,
       width:"60vw",
       height:"70vh",
       role: 'dialog',
    });
    dialogRef.afterClosed().subscribe(result => {
    
    });
  }

  onDelete(item){
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: { 
        TITLE:`Are you sure to delete ${item?.db_name}?`,
        item:null,
       },
       disableClose: true,
       role: 'dialog',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && result !== 'no') {
        this.dbStore.delete(item,cb=>{
          console.log(cb)
        })
      }
    });
  }

}
