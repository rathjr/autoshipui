import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UserStore } from 'src/app/store/user.store';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    public userStore:UserStore
  ) { }
  key:string;
  isOwner=false
  ngOnInit(): void {
    this.route.params.forEach(param => {
      this.key = param.id;
      this.key==this.userStore.userID?this.isOwner=true:this.isOwner=false;
      console.log(this.key)
    });
  }

}
