import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { first } from 'rxjs/operators';
import  auth  from 'firebase/app';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  constructor(
    private afAuth: AngularFireAuth,
    private router: Router,
    ) { }

  isLoggedIn() {
    return this.afAuth.authState.pipe(first()).toPromise();
  }
  
  authRef() {
    return this.afAuth
  }
  getUser(): Promise<any> {
    return this.afAuth.authState.pipe(first()).toPromise();
}
 // Sign in with Google
  GoogleAuth() {
  return this.AuthLogin(new auth.auth.GoogleAuthProvider());
}  
  GithubAuth(){
  return this.AuthLogin(new auth.auth.GithubAuthProvider())
}
// Auth logic to run auth providers
AuthLogin(provider) {
  return this.afAuth.signInWithPopup(provider)
  .then(() => {
    this.router.navigate(['/dashboard']);
  }).catch((error) => {
      console.log(error)
  })
}

  emailSignUp(email: string, password: string) {
    return this.afAuth.createUserWithEmailAndPassword(email, password);
  }

  emailSignIn(email: string, password: string) {
    return this.afAuth.signInWithEmailAndPassword(email, password);
  }

  signOut() {
    return this.afAuth.signOut();
  }

}
