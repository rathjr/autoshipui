import { Injectable } from '@angular/core';
import { CanActivate,Router} from '@angular/router';
import { AuthService} from './auth.service'

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private auth: AuthService,private router: Router) {}

  async canActivate(): Promise<boolean> {
      const user = await this.auth.getUser();
      const loggedIn = !!user;
      if (!loggedIn) {
        this.router.navigate(['/login']);
      }
      return loggedIn;
  }
}