
export const deployStatus = [
    { key: 1, text: 'pending'},
    { key: 2, text: 'success' },
    { key: 3, text: 'error'},
    { key: 4, text: 'failed'},
  ]
  export const appStatus = [
    { key: 1, text: 'Running'},
    { key: 2, text: 'Stopped' },
  ]
  export const recordActive = [
    { key: 1, text: 'active'},
    { key: 2, text: 'deleted' },
  ]

  export const buildTool = [
    { key: 1, text: 'Maven'},
    { key: 2, text: 'Gradle' },
  ]

  export const nodejsVersion = [
    { key: 1, text: '17.3.0'},
    { key: 2, text: '16.13.1' },
    { key: 3, text: '15.14.0' },
    { key: 4, text: '14.18.2' },
    { key: 5, text: '13.13.0' },
    { key: 6, text: '12.22.8' },
    { key: 7, text: '11.9.0' },
    { key: 8, text: '10.20.0' },
    { key: 9, text: '9.11.2' },
  ]
  
export const jdkVersion = [
  { key: 1, text: '17'},
  { key: 2, text: '16' },
  { key: 3, text: '15' },
  { key: 4, text: '14' },
  { key: 5, text: '13' },
  { key: 6, text: '12' },
  { key: 7, text: '11' },
  { key: 8, text: '10' },
  { key: 9, text: '9' },
  { key: 8, text: '8' },

]