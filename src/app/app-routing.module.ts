import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { DashboardMainComponent } from './pages/dashboard-main/dashboard-main.component';
import { AuthLayoutComponent } from './shared/auth-layout/auth-layout.component';
import { DashboardLayoutComponent } from './shared/dashboard-layout/dashboard-layout.component';
const routes: Routes = [
  {
    path: "login",
    component: AuthLayoutComponent,
    children: [
      { path: "", component: AuthLayoutComponent,
      children: [
        { path: "", redirectTo: "", pathMatch: "full" },
        // { path: "", component: DashboardComponent },
        // { path: "listing/:id", component: ListingRouterComponent },
      ]
       },
    ]
  },
  {
    path: "",
    component: DashboardLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      { path: "", redirectTo: "dashboard", pathMatch: "full" },
      { path: "dashboard", component: DashboardMainComponent },
      { path: "account/:id", component: UserProfileComponent },
      // { path: "listing/:id", component: ListingRouterComponent },
    ]
     }
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
