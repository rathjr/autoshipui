import { Component, OnInit } from '@angular/core';
import { DatabaseStore } from 'src/app/store/database.store';
import { FrontEndStore } from 'src/app/store/frontend.store';

@Component({
  selector: 'app-dashboard-main',
  templateUrl: './dashboard-main.component.html',
  styleUrls: ['./dashboard-main.component.scss']
})
export class DashboardMainComponent implements OnInit {
  constructor(
    public frontEndStore:FrontEndStore,
    ) { }

  ngOnInit(): void {
    this.frontEndStore.getDeploySize()
    this.frontEndStore.getDatabaseSize()
  }

}
