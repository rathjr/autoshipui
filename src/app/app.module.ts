import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { BrowserModule } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { AppComponent } from './app.component';
import { APP_STORES } from './store/app.store';
import { APP_SERVICES } from './services/app.services';
import { MobxAngularModule } from 'mobx-angular';
import { MaterialModule } from './Material_Modules/material_modules';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DashboardLayoutComponent } from './shared/dashboard-layout/dashboard-layout.component';
import { HeaderComponent } from './shared/header/header.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from './auth/auth.guard';
import { AuthLayoutComponent } from './shared/auth-layout/auth-layout.component';
import { AuthenticationComponent } from './pages/authentication/authentication.component';
import { RegisterComponent } from './pages/register/register.component';
import { DashboardMainComponent } from './pages/dashboard-main/dashboard-main.component';
import { AppListingItemComponent } from './components/app-listing-item/app-listing-item.component';
import { AddDatabaseComponent } from './dialogs/add-database/add-database.component';
import { AddApplicationsComponent } from './dialogs/add-applications/add-applications.component';
import { DatabaseDetailComponent } from './dialogs/database-detail/database-detail.component';
import { AppDetailComponent } from './dialogs/app-detail/app-detail.component';

import { DatabaseListingItemComponent } from './components/database-listing-item/database-listing-item.component';
import { ApplicationComponent } from './components/application/application.component';
import { DatabaseComponent } from './components/database/database.component';
import { EmptyLabelComponent } from './components/empty-label/empty-label.component';
import { ConfirmDialogComponent } from './dialogs/confirm-dialog/confirm-dialog.component';
import { DatabaseCardComponent } from './components/database-card/database-card.component';
import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';
import { DashboardCardComponent } from './components/application-card/dashboard-card.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { HttpClientModule } from '@angular/common/http';
import { SafePipe } from './pipes/safeHtml.pipe';

@NgModule({
  declarations: [
    SafePipe,
    AppComponent,
    DashboardLayoutComponent,
    HeaderComponent,
    SidebarComponent,
    AuthLayoutComponent,
    AuthenticationComponent,
    RegisterComponent,
    DashboardCardComponent,
    DashboardMainComponent,
    AppListingItemComponent,
    AddDatabaseComponent,
    AddApplicationsComponent,
    DatabaseDetailComponent,
    AppDetailComponent,
    DatabaseListingItemComponent,
    ApplicationComponent,
    DatabaseComponent,
    EmptyLabelComponent,
    ConfirmDialogComponent,
    DatabaseCardComponent,
    LoadingSpinnerComponent,
    UserProfileComponent,
  ],
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    MaterialModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    MobxAngularModule,
    HttpClientModule,

  ],
  providers: [
    APP_STORES,
    APP_SERVICES,
    AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
