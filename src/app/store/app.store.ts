import { DatabaseStore } from "./database.store";
import { FrontEndStore } from "./frontend.store";
import { DeploySpringStore } from "./deploy-spring.store";
import { UserStore } from "./user.store";

export const APP_STORES = [
    DatabaseStore,
    FrontEndStore,
    DeploySpringStore,
    UserStore
];
