import { observable, action } from 'mobx-angular';
import { Injectable } from '@angular/core';
import { DataService } from '../services/data.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable()
export class DatabaseStore {
  @observable 
  public data=[];
  public empty=false;
  public loading=false;
  @observable 
  public process=false;
  constructor(
    private ds:DataService,
    private httpClient:HttpClient){
  }
  @action
  firebaseKey(){
    return this.ds.firestoreKey()
  }
  @action
  dateTime(){
    return new Date()
  }
  @action
  add(item:any){
    return  this.ds.databaseRef().doc(item.key).set(item).then(()=>{
      let httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache'
    })
      if(item.db_type=='PostgreSQL'){        
        let response=this.httpClient.post(`http://34.126.133.229:8081/api/v3/postgers/1/${item?.key}/${item?.db_name}/${item?.username}/${item?.password}/${item?.connection_limit}/${item?.port}`,httpHeaders)
        response.subscribe((data)=>console.log(data))
      }
      if(item.db_type=='MySQ'){        
        let response=this.httpClient.post(`http://34.126.133.229:8081/api/v3/mySql/1/${item?.key}/${item?.db_name}/${item?.username}/${item?.password}/${item?.connection_limit}/${item?.port}`,httpHeaders)
        response.subscribe((data)=>console.log(data))
      }
    })
  }
  @action
  fetchData(owner_key:string){
    this.loading=true;
  return  this.ds.databaseRefbyOwner(owner_key).valueChanges().subscribe(docs=>{
      this.data=docs;
      this.empty=docs.length===0;
      this.loading=false;
    })
  }
  @action
  update(key,item,callback){
      this.ds.databaseRef().doc(key).update(item).then(()=>{
          callback(item)
      }).catch(error=>{
          callback(error)
      })
  }
  @action
  delete(item,callback){
    this.process=true;
    this.ds.databaseRef().doc(item.key).delete().
    then(()=>{
      this.process=false;
      callback(true,null)
    }).catch((error)=>{
      this.process=false;
      callback(false,error)
    })
  }
}