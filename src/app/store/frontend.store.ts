import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { action, observable } from 'mobx';
import { IFrontEnd } from '../interfaces/frontend.interface';
import { DataService } from '../services/data.service';
import { UserStore } from './user.store';

@Injectable({
  providedIn: 'root'
})
export class FrontEndStore {

  @observable 
  public data=[];
  public filterData=[]
  public empty=false;
  public loading=true;
  public deploySize=0;
  public databaseSize=0
  angularEndPoint:`http://34.126.133.229:8081//api/v3/angular/1`
  totalSize:number;
  @observable 
  public process=false;
  constructor(
    private ds:DataService,
    private userStore:UserStore,
    private httpClient:HttpClient
    ){
  }
  @action
  firebaseKey(){
    return this.ds.firestoreKey()
  }
  @action
  dateTime(){
    return new Date()
  }
  @action
  add(item:any){
    this.loading=true
    return  this.ds.appilcationRef().doc(item.key).set(item).then(()=>{
      let httpHeaders = new HttpHeaders({
        'Content-Type': 'application/json',
        'Cache-Control': 'no-cache'
    })
    console.log(item.build_file)
      if(item.language=='Angular'){
      let response=this.httpClient.post(`http://34.126.133.229:8081/api/v3/angular/1/${item?.key}/{linkGit}?linkGit=${item?.source_code}`,httpHeaders)
      response.subscribe((data)=>console.log(data))
      }
      if(item.language=='Spring' && item.build_file=="War"){        
        let response=this.httpClient.post(` http://34.126.133.229:8081/api/v3/war/1/${item?.key}/{linkGit}?linkGit=${item.source_code}`,httpHeaders)
        response.subscribe((data)=>console.log(data))
      }
      if(item.language=='Spring' && item.build_file=="Jar"){        
        let response=this.httpClient.post(` http://34.126.133.229:8081/api/v3/jar/1/${item?.key}/{linkGit}?linkGit=${item.source_code}`,httpHeaders)
        response.subscribe((data)=>console.log(data))
      }
      if(item.language=='ReactJS'){        
        let response=this.httpClient.post(`http://34.126.133.229:8081/api/v3/reactjs/1/${item?.key}/{linkGit}?linkGit=${item?.source_code}`,httpHeaders)
        response.subscribe((data)=>console.log(data))
      }
    })
  }
  @action
  fetchData(owner_key:string){
    this.loading=true;
  return  this.ds.applicationRefbyOwner(owner_key).valueChanges().subscribe(docs=>{
      this.data=docs;
      this.empty=docs.length===0;
      this.loading=false;
    })
  }
  @action
  fetchDataByLanguage(owner_key:string,language:string){
    this.loading=true;
  return  this.ds.applicationRefbyOwnerAndFilterApp(owner_key,language).valueChanges().subscribe(docs=>{
      this.filterData=docs;
      this.empty=docs.length===0;
      this.loading=false;
    })
  }
  @action
  async getDeploySize(){
    this.loading=true;
    if (await this.userStore.userID){
      this.ds.getDocumentSized(this.userStore.userID).get().subscribe((doc)=>{
        this.deploySize=doc.size;
        this.loading=false;
      })
    }
  }

  @action
 async  getDatabaseSize(){
    this.loading=true;
    if (await this.userStore.userID){
      this.ds.getDatabaseSized(this.userStore.userID).get().subscribe((doc)=>{
        this.databaseSize=doc.size;
        this.loading=false;
      })
    }
  }

  @action
  update(key,item,callback){
      this.ds.appilcationRef().doc(key).update(item).then(()=>{
          callback(item)
      }).catch(error=>{
          callback(error)
      })
  }
  @action
  delete(item,callback){
    this.process=true;
    this.ds.appilcationRef().doc(item.key).delete().
    then(()=>{
      this.process=false;
      this.getDeploySize()
      callback(true,null)
    }).catch((error)=>{
      this.process=false;
      callback(false,error)
    })
  }
}