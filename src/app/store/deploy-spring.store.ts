import { FrontEndStore } from 'src/app/store/frontend.store';
import { Injectable } from '@angular/core';
import { action, observable } from 'mobx';
import { DataService } from '../services/data.service';

@Injectable({
  providedIn: 'root'
})
export class DeploySpringStore {
  @observable 
  public data=[];
  public empty=false;
  public loading=false;
  @observable 
  public process=false;
  constructor(
    private ds:DataService,
    private frontEndStore:FrontEndStore
    ){
  }
  @action
  firebaseKey(){
    return this.ds.firestoreKey()
  }
  @action
  dateTime(){
    return new Date()
  }
  @action
  add(item:any){
    return  this.ds.appilcationRef().doc(item.key).set(item);
  }
  @action
  fetchData(owner_key:string){
    this.loading=true;
    this.ds.applicationRefbyOwner(owner_key).valueChanges().subscribe(docs=>{
      this.data=docs;
      this.empty=docs.length===0;
      this.loading=false;
    })
  }
  @action
  update(key,item,callback){
      this.ds.appilcationRef().doc(key).update(item).then(()=>{
          callback(item)
      }).catch(error=>{
          callback(error)
      })
  }
  @action
  delete(item,callback){
    this.process=true;
    this.ds.appilcationRef().doc(item.key).delete().
    then(()=>{
      this.frontEndStore.getDatabaseSize()
      this.process=false;
      callback(true,null)
    }).catch((error)=>{
      this.process=false;
      callback(false,error)
    })
  }
}