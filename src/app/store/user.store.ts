import { Injectable } from '@angular/core';
import { action, observable } from 'mobx';
import { AuthService } from '../auth/auth.service';
import { IUser } from '../interfaces/user.interface';
import { DataService } from '../services/data.service';

@Injectable({
  providedIn: 'root'
})
export class UserStore {

  @observable 
  public data=[];
  public filterData=[]
  public empty=false;
  public userID=null
  public loading=false;
  @observable 
  public process=false;
  constructor(private ds:DataService,private auth:AuthService){
  }
  @action
 async getCurrentUser(){
  let user= await this.auth.getUser()
    if(user)
      this.userID=user.uid
      this.checkUserExist()
      const docs = this.ds.pushToArray(
        await this.ds
          .userRef(user.uid)
          .get()
          .toPromise()
      );
      this.data=docs
    }
  async checkUserExist(){
    let user= await this.auth.getUser()
      if(user)
        this.userID=user.uid
        let user_info:IUser={
          uid:user.uid,
          displayName:user.displayName,
          email:user.email,
          emailVerified:user.emailVerified,
          photoURL:user.photoURL,
          isAdmin:false,
          career:null,
          skills:null,
          update_date:new Date()
        }
        this.ds.userRef(user.uid).doc(user.uid).get().subscribe((doc)=>{
          if(doc.exists){
            console.log('User existed!')
           }else{
            this.ds.userRef(user.uid).doc(user.uid).set(user_info)
           }
        })
    }

    @action
    update(key,item,callback){
        this.ds.UserDataRef().doc(key).update(item).then(()=>{
            callback(item)
        }).catch(error=>{
            callback(error)
        })
    }
}