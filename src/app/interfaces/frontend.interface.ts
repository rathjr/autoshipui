export interface IFrontEnd {
    key:string;
    owner_key:string;
    app_name:string;
    container_name:string;
    description:string;
    language:string;
    source_code:string;
    nodejs_version:string;
    record_active_status:boolean;
    status: any;
    ip:string;
    port:string;
    logs:any;
    create_date?: any;
    create_by?: any;
    update_date?: any;
    update_by?: any;
}

export interface IFrontEndDetail {
    key:string;
    owner_key:string;
    app_name:string;
    language:string;
    source_code:string;
    nodejs_version:string;
    
    ip:string;
    port:string;

    status: any;
    logs:any;
    create_date?: Date;
    create_by?: object;
    update_date?: Date;
    update_by?: object;
}