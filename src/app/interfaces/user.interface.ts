export interface IUser {
    uid:string;
    displayName:string;
    email:string;
    emailVerified:string;
    photoURL:string;
    isAdmin:boolean;
    career:any;
    skills:any;
    update_date?: any;
}