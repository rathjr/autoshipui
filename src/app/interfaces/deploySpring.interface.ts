export interface IDeploySpring {
    key:string;
    owner_key:string;
    app_name:string;
    description:string;
    language:string;
    source_code:string;
    build_tool:string;
    jdk_version:string;
    container_name:string;
    build_file:string;
    record_active_status:boolean;
    ip:string;
    port:string;
    status: any;
    logs:any;
    create_date?: any;
    create_by?: any;
    update_date?: any;
    update_by?: any;
}
