export interface IDomain {
    key:string;
    domain_name:string;
    description?:string;

    status: any;
    create_date?: Date;
    create_by?: object;
    update_date?: Date;
    update_by?: object;
}