export interface IDatabase {
    key: string;
    owner_key:string;
    db_name: string;
    username:string;
    password:string;
    container_name:string;
    record_active_status:boolean;
    connection_limit:number;
    description?: string;
    ip:string;
    port:string;
    db_type:string;
    status: any;
    create_date?: any;
    create_by?: any;
    update_date?: any;
    update_by?: any;
  }

  export interface IDatabaseDetail {
    key: string;
    dbname: string;
    username:string;
    password:string;
    description?: string;
    ip:string;
    port:number;
    status: any;
    create_date?: any;
    create_by?: any;
    update_date?: any;
    update_by?: any;
  }