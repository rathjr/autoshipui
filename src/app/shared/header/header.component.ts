import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth/auth.service';
import { UserStore } from 'src/app/store/user.store';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  constructor(
    private authService:AuthService,
    private router:Router,
    public userStore:UserStore,
    ) { }

  ngOnInit(): void {
    if(this.userStore.userID){
      console.log(this.userStore.data)  
    }
  }
  onSignOut(){
    this.authService.signOut().finally(()=> this.router.navigate(['/login']))
  }
  onViewDashboard(){
    this.router.navigate([`/dashboard`])
  }
  onViewProfile(){
    if(this.userStore.userID){
      this.router.navigate([`/account/${this.userStore.userID}`])
    }
  }
}
