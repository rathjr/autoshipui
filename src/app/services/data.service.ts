import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { IDatabase, IDatabaseDetail } from '../interfaces/db.interface';
import  firestore  from 'firebase';
import 'firebase/firestore';
import { IFrontEnd } from '../interfaces/frontend.interface';
import { environment } from 'src/environments/environment.prod';
import { IUser } from '../interfaces/user.interface';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private db: AngularFirestore) { }
  firestoreKey(){
    return this.db.createId()
  }
  appilcationRef() {
    return this.db.collection<IFrontEnd>(environment.COLLECTION.deployment, ref => ref.orderBy('create_date'));
  }
  UserDataRef() {
    return this.db.collection<IUser>(environment.COLLECTION.deployment);
  }

  userRef(uid) {
    return this.db.collection<IUser>(environment.COLLECTION.users, ref => ref.where('uid','==',uid));
  }

  pushToArray(snapshot: firestore.firestore.QuerySnapshot): Array<any> {
    if (snapshot.empty) return [];
    return snapshot.docs.map(m => ({ ...m.data(), id: m.id }));
  }
  
  applicationRefbyOwner(owner_key: string) {
    return this.db.collection<IFrontEnd>(environment.COLLECTION.deployment, ref =>
      ref.where('owner_key', '==', owner_key).where('record_active_status','==',true).orderBy('create_date'));
  }
  getDocumentSized(owner_key: string){
   let docs=this.db.collection<any>(environment.COLLECTION.deployment, ref =>
      ref.where('owner_key', '==', owner_key).where('record_active_status','==',true));
      return docs;
  }
  getDatabaseSized(owner_key: string){
    let docs=this.db.collection<any>(environment.COLLECTION.database, ref =>
       ref.where('owner_key', '==', owner_key).where('record_active_status','==',true));
       return docs;
   }

  applicationRefbyOwnerAndFilterApp(owner_key: string,language:string) {
    return this.db.collection<any>(environment.COLLECTION.deployment, ref =>
      ref.where('owner_key', '==', owner_key).where('language','==',language).where('record_active_status','==',true).orderBy('create_date'));
  }

  databaseRef() {
    return this.db.collection<IDatabase>(environment.COLLECTION.database, ref => ref.where('record_active_status','==',true).orderBy('create_date'));
  }
  databaseRefbyOwner(owner_key: string) {
    return this.db.collection<IDatabaseDetail>(environment.COLLECTION.database, ref =>
      ref.where('owner_key', '==', owner_key).where('record_active_status','==',true).orderBy('create_date'));
  }
 
}
