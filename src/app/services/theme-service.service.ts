import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  white: string = '#ffffff';
  black: string = '#141313';

  private _themeDark: Subject<boolean> = new Subject<boolean>();

  isThemeDark = this._themeDark.asObservable();

  constructor() { }

  setDarkTheme(isThemeDark: boolean) {
    this._themeDark.next(isThemeDark);

    if (isThemeDark == true) {
      document.documentElement.style.setProperty('background', this.black);
      localStorage.setItem('dark', 'true');
    }
    else {
      document.documentElement.style.setProperty('background', this.white);
      localStorage.setItem('dark', 'false');
    }
  }

}