// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    projectId: 'autoshipkh',
    appId: '1:63150366999:web:0800244ca7762c3bfad752',
    storageBucket: 'autoshipkh.appspot.com',
    locationId: 'asia-southeast1',
    apiKey: 'AIzaSyAfyTBGBSw3-o-w7L3t1lVvACbge4hLyXY',
    authDomain: 'autoshipkh.firebaseapp.com',
    messagingSenderId: '63150366999',
  },
  COLLECTION:{
    database:'databases',
    deployment:'deployments',
    users:'users'
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
