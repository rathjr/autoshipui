import * as functions from "firebase-functions";
import * as admin from "firebase-admin";
import * as express from "express";
import * as bodyParser from "body-parser";
admin.initializeApp(functions.config().firebase);
const app =express();
const main =express(); 
main.use("/api/v1", app);
main.use(bodyParser.json());
main.use(bodyParser.urlencoded({ extended: false }));
const db = admin.firestore();
const deploymentCollection = "deployments";
interface User {
    key: String,
    logs: String,
    port:number,
    ip:string,
    status:object;
}
app.post("/app", async (req, res) => {
    try {
        const user: User = {
            key: req.body["key"],
            logs: req.body["logs"],
            port:req.body["port"],
            ip:req.body["ip"],
            status:{
                id:2,
                text:'success'
            }
        }
        await db.collection(deploymentCollection).doc(req.body["key"]).set(user,{merge:true});
        res.status(201).send("Updated applicaton ID : "+req.body["key"]);
    } catch (error) {
        res.status(400).send("Key and logs required!")
    }
});
export const deployment = functions.https.onRequest(app);

